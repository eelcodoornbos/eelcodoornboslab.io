---
title: 3D orbit simulation using WebGL
date: 2020-05-26 22:19:50
tags:
---
{% raw %}
    <script type="text/javascript" src="/js/astro.js"></script> <!-- My own astrodynamics library -->
    <script type="text/javascript" src="/lib/sylvester.src.js"></script> <!-- Vector library -->
    <script type="text/javascript" src="/lib/three.js"></script>  <!-- 3D WebGL rendering library -->
    <script type="text/javascript" src="/lib/OrbitControls.js"></script> <!-- Library for controlling the camera in the 3D scene with the mouse or trackpad -->
    <script type="text/javascript" src="/lib/threex.keyboardstate.js"></script> <!-- Keyboard input for controlling satellite thrusters -->
    <script type="text/javascript" src="/lib/dat.gui.js"></script> <!-- Library for making a GUI panel for manipulating the variables used in the simulation -->
    <link type="text/css" rel="stylesheet" href="/css/dat-gui-light.css"> <!-- Style for the GUI panel -->
{% endraw %}
This is a test of embedding Javascript in a post on Hexo.

{% raw %}
<!-- Container for the orbit simulation -->
<div id='orbitsimulation' style='position: relative; display: inline-block; width: 100%; height: 600px; border: 1px solid black; padding: 0px;'>
  <!-- Container for the GUI panel -->
  <div id='datgui' style='position: absolute; top: 0px; right: 0px;'></div>
</div>
<!-- Execute the code that sets up and runs the simulation! -->
<script type="text/javascript" src="/js/orbit_integrator_3d.js"></script>
{% endraw %}