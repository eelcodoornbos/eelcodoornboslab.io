---
title: Installing scientific library packages in Blender's bundled Python
date: 2020-05-21 15:28:59
tags:
---

[Installing pip](https://pip.pypa.io/en/stable/installing/)
{% codeblock lang:bash %}
cd ~/Downloads
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
{% endcodeblock %}

Then install pip 
{% codeblock lang:bash %}
cd /Applications/Blender.app/Contents/Resources/2.82/python/bin
./python ~/Downloads/get-pip.py
{% endcodeblock%}

Installing Blender packages
{% codeblock lang:bash %}
cd /Applications/Blender.app/Contents/Resources/2.82/python/bin
./pip install pandas scipy matplotlib astropy netcdf4 apexpy
{% endcodeblock%}
