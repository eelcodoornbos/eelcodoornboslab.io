---
title: about
date: 2020-05-21 11:41:58
---
# About me

Hello, and welcome to the personal website of Eelco Doornbos. I am a researcher working on space weather. I have a passion for visualising data and model output, to help in understanding the complex processes in geospace that affect space weather. I have set up this website to share what I have learned on this topic.

# About this website
This site was built using the [Hexo framework](https://hexo.io) for static sites. It makes use of the [Cactus theme for Hexo](https://github.com/probberechts/hexo-theme-cactus).
