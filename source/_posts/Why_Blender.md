---
title: Why Blender?
date: 2020-05-28 10:01:52
tags:
---
## My journey towards using Blender

### Starting out with 2D
When I became interested in visualizing scientific data and models in 3D, I already had a lot of experience working with 2D scientific graphics software, such as the underappreciated [Generic Mapping Tools (GMT)](https://www.generic-mapping-tools.org), wich I used extensively for my PhD thesis [(PDF download link)](https://repository.tudelft.nl/islandora/object/uuid:33002be1-1498-4bec-a440-4c90ec149aea/datastream/OBJ/download). I even made fake 3D movies in GMT, using its orthographic map projection. Like this rather complex one:
{% vimeo 264594121 %} 

### Dedicated scientific 3D visualization software
I briefly looked into solutions like [Paraview](https://www.paraview.org), [Mayavi](https://docs.enthought.com/mayavi/mayavi/) and [VTK](https://vtk.org). And while these are undoubtedly powerful packages, there was something lacking, both in the graphical output and training material. The output looked very sterile to me. Have a look at some of the example galleries behind these links. A certain cinematic quality and hint of an artists' touch was missing. And the training videos were just plain boring.

### Into the cinematic 3D rendering world
I then tried out [Cinema4D](https://www.maxon.net/en-us/products/cinema-4d/overview/). A ['lite' version](https://helpx.adobe.com/after-effects/how-to/introducing-cinema4d-lite-aftereffects.html) was included in my institute's license of the [Adobe Creative Cloud](https://www.adobe.com/creativecloud.html), as part of [After Effects](https://www.adobe.com/products/aftereffects.html). But the software felt constraining to me. It was difficult to find examples of the kind of things I wanted to do with it, and I was never motivated to dive in very deep.

### Blender
I eventually settled on learning [Blender](https://www.blender.org). Blender is intended first and foremost as a powerful tool for artists. However, Blender is also set up to be extremely flexible and extensible, making it potentially very suitable for scientific visualization as well. This combination allows the scientific visualizer to bring cinematic quality and artistic touches to their work.

Blender is also free and open source software, with a very enthusiastic and engaged user community. To me, this is a huge benefit compared to other software. Not only because of the low cost and excellent community, but also because it makes me feel that the effort that I put into learning and extending Blender myself, will not be wasted, should licensing terms change or expire.

{% img /images/daedalus_magnetosphere_nightside.png '"Blender render of the geomagnetic field and the Deadalus orbit"' %}
