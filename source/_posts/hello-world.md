---
title: Hello world, hello geospace!
date: 2020/05-24 18:10:00
---
Welcome to this website! My intention is to use this space on the internet to document some of the work that I do on visualizing geospace. Hopefully this information will be useful to others who might want to do the same. In this first post, let me give a very basic introduction of what geospace means, and why I think it is important to work on visualizations of geospace.

## What is geospace?
Geospace can be defined as the region of outer space that is close to Earth, and that is strongly influenced by the presence of our planet. It includes the upper atmosphere, the ionosphere, and the magnetosphere. In this region, the Earth's gravitational pull and magnetic field interact in complex ways with solar radiation and solar wind.

## Why is geospace important?
Geospace is the region where space weather impacts on our infrastructure originate. For example, variations in the electron density in the ionosphere can affect radio signals between satellites and ground, used for communication and navigation. In certain situations this can cause significant degradations of the signal and even temporary loss of these services.
In addition, currents of charged particles in the magnetosphere and ionosphere can induce currents in electrical power grids, which can cause problems in management of power, and in extreme cases even damage to power infrastructure.


## Why work on visualizing geospace?
Almost by definition, this region is invisible to us. It is space after all. But there are some very interesting exceptions to this, the most important and exciting of which is the aurora or northern lights. It turns out that the aurora is just one element of a complicated system of processes. Obseratories on satellites and on the ground can provide important scientific and operational data on these processes. Models can be used together with these observations to help us make sense of it. Scientific visualization of these data and models can be an important tool in helping us improve our understanding, as well as communicate this within the scientific community and towards a wider audience.  
