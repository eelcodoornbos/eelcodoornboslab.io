---
title: Learning Blender for scientists
date: 2020-06-03 10:41:21
tags:
---

## Starting to learn Blender
Like all powerful tools, there is a learning curve to using Blender. This was for a long time a big obstacle for me. Back when I started to learn it in earnest, Blender was at version 2.79. Since then, with the release of [version 2.80](https://www.blender.org/download/releases/2-80/) in the summer of 2019, the user interface has been greatly improved, getting rid of a lot of some of its weird user interface quirks, and replacing it with a much more friendly and thoughtful interface. This is very good news to those who want to start learning Blender now.

So what's the best way to start learning? My advice is to not start right away on trying to make scientific visualizations, but first learn the software the way any aspiring 3D artist would. The reason for this is that learning materials for scientific use of Blender or not yet well developed. However, there are many books and video tutorials available for artists, which teach the basics of Blender, that will come in very handy for later scientific use as well. Some of these are mediocre (and these shall not be mentioned here), but many are excellent. These include the official 
[Blender Fundamentals playlist](https://www.youtube.com/playlist?list=PLa1F2ddGya_-UvuAqHAksYnB0qL9yWDO6). However, my favorite way to start learning is Blender Guru's very popular [Blender Beginner Tutorial Series](https://www.youtube.com/playlist?list=PLjEaoINr3zgEq0u2MzVgAaHEBt--xLB6U), also known as the "Donut Tutorial". Andrew Price's laid-back, informal and hands-on style of presenting all of the main features of the software, at exactly the right pace, really makes learning Blender a pleasure.
{% youtube NyJWoyVx_XI %}

## Towards more advanced use of Blender
Blender Guru also has several more advanced tuturials, including this rather long one to [render the Earth](https://www.youtube.com/watch?v=9Q8PwcDzb8Y)
{% youtube 9Q8PwcDzb8Y %}
This video is now rather old, and makes use of an older version of the Blender user interface. I also have an issue with the unrealistic angle of the sunlight, but nevertheless this video gives a nice idea of how to work with textures and materials based on satellite imagery of the Earth, to get really impressive looking visuals.

After finishing the Donut tutorial, you will also be in the right place to be able to follow interesting tutorials by other YouTubers. Some of my favourites are listed below:
- [Ducky 3D](https://www.youtube.com/channel/UCuNhGhbemBkdflZ1FGJ0lUQ)
- [CGMatter](https://www.youtube.com/channel/UCy1f4m64dwCwk8CBZ_vHfPg)
- [Grant Abbitt](https://www.youtube.com/user/mediagabbitt)

## Scientific use of Blender
To get into scientific use of Blender, some programming is required. Fortunately, Blender has a very extensive and well-documented [Python programming interface](https://docs.blender.org/api/current/index.html). If you want to continue learning from videos, however, the 
[Scripting for Artists](https://www.youtube.com/playlist?list=PLa1F2ddGya_8acrgoQr1fTeIuQtkSd6BW), by the Blender Institute's Dr. Sybren A. Stüvel is a very good place to continue. [The full series of videos](https://cloud.blender.org/p/scripting-for-artists/) is available only to Blender Cloud members, however the most recent updates, making use of Blender 2.8, are available for free on YouTube. [These YouTube videos](https://www.youtube.com/playlist?list=PLa1F2ddGya_8acrgoQr1fTeIuQtkSd6BW) should be more than sufficient to get you going. This tutorial series is still primarily aimed at artists, but the techniques you learn in this video series are essential for scientists as well. 

In future blog posts on this website, I will give some examples of how I use Blender myself for scientific visualisation. The tutorials listed here should be a great place to start learning, to bring you to a place to follow along with those future posts.
